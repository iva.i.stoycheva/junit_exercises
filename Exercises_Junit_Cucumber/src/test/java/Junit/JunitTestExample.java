package Junit;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class JunitTestExample {

    public String message = "Guru99";
    JUnitMessage jUnitMessage = new JUnitMessage(message);

   // @Ignore
    @Test
    public void testJUnitMessage(){
        System.out.println("junit message is printing");
        assertEquals(message, jUnitMessage.printMessage());
    }

    @Test
    public void testJUnitHiMessage(){
        message = "Hi" + message;
        System.out.println("junit hi message is printing ");

        assertEquals(message, jUnitMessage.printHiMessage());
    }
}
