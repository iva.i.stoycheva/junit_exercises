package Junit;

import org.junit.Assert;
import org.junit.Test;

import static junit.framework.TestCase.assertSame;
import static org.junit.Assert.*;

public class Junit4AssertionTest {

    @Test
    public void testAssert(){
        //variable declaration

        String string1 = "Junit";
        String string2 = "Junit";
        String string3 = "test";
        String string4 = "test";
        String string5 = "null";

        int variable2=2;
        int variable1=1;
        int [] airethematicArray1 = {1, 2, 3};
        int [] airethematicArray2 = {1, 2, 3};

        // assert statements
        assertEquals(string1, string2);

        assertSame(string3, string4);

        assertNotSame(string1, string3);

        assertNotNull(string1);

        assertNull(string5);

        assertTrue(variable1<variable2);

        assertArrayEquals(airethematicArray1, airethematicArray2);

    }
}
